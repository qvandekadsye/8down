﻿namespace _8down
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.notify8Down = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eteindreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redemarrerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mettreEnVeilleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fermerLaSessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNotify.SuspendLayout();
            this.SuspendLayout();
            // 
            // notify8Down
            // 
            this.notify8Down.ContextMenuStrip = this.menuNotify;
            this.notify8Down.Text = "notifyIcon1";
            this.notify8Down.Visible = true;
            // 
            // menuNotify
            // 
            this.menuNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eteindreToolStripMenuItem,
            this.redemarrerToolStripMenuItem,
            this.mettreEnVeilleToolStripMenuItem,
            this.fermerLaSessionToolStripMenuItem});
            this.menuNotify.Name = "menuNotify";
            this.menuNotify.Size = new System.Drawing.Size(165, 114);
            // 
            // eteindreToolStripMenuItem
            // 
            this.eteindreToolStripMenuItem.Name = "eteindreToolStripMenuItem";
            this.eteindreToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.eteindreToolStripMenuItem.Text = "Eteindre";
            this.eteindreToolStripMenuItem.Click += new System.EventHandler(this.eteindreToolStripMenuItem_Click);
            // 
            // redemarrerToolStripMenuItem
            // 
            this.redemarrerToolStripMenuItem.Name = "redemarrerToolStripMenuItem";
            this.redemarrerToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.redemarrerToolStripMenuItem.Text = "Redémarrer";
            this.redemarrerToolStripMenuItem.Click += new System.EventHandler(this.redemarrerToolStripMenuItem_Click);
            // 
            // mettreEnVeilleToolStripMenuItem
            // 
            this.mettreEnVeilleToolStripMenuItem.Name = "mettreEnVeilleToolStripMenuItem";
            this.mettreEnVeilleToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.mettreEnVeilleToolStripMenuItem.Text = "Mettre en veille";
            // 
            // fermerLaSessionToolStripMenuItem
            // 
            this.fermerLaSessionToolStripMenuItem.Name = "fermerLaSessionToolStripMenuItem";
            this.fermerLaSessionToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.fermerLaSessionToolStripMenuItem.Text = "Fermer la session";
            this.fermerLaSessionToolStripMenuItem.Click += new System.EventHandler(this.fermerLaSessionToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 90);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuNotify.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notify8Down;
        private System.Windows.Forms.ContextMenuStrip menuNotify;
        private System.Windows.Forms.ToolStripMenuItem eteindreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redemarrerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mettreEnVeilleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fermerLaSessionToolStripMenuItem;
    }
}

