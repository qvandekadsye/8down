﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace _8down
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.notify8Down.Icon = Properties.Resources.test;
            this.notify8Down.BalloonTipIcon = ToolTipIcon.Info;
            this.notify8Down.BalloonTipTitle = "8down";
            this.notify8Down.BalloonTipText = "8down est lancée.";
            this.notify8Down.ShowBalloonTip(0);
            this.Visible = true;
            
            
        }

        private void eteindreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(@"C:\Windows\System32\shutdown.exe","/s /t 00");
            
        }

        private void redemarrerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(@"C:\Windows\System32\shutdown.exe", "/r /t 00");
        }

        private void fermerLaSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(@"C:\Windows\System32\shutdown.exe", "/l");
        }
    }
}
