﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using System.IO;
namespace _8down
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        
        
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            NotifyIcon notify8Down = new NotifyIcon();
            ContextMenuStrip contxtmnu = new ContextMenuStrip();

            ToolStripMenuItem itm0 = new ToolStripMenuItem("Eteindre l'ordinateur");
            ToolStripMenuItem itm1 = new ToolStripMenuItem("Redémarrer l'ordinateur");
            ToolStripMenuItem itm2=  new ToolStripMenuItem("Options de redémarrage avancé");
            ToolStripMenuItem itm4 = new ToolStripMenuItem("Fermer la session");
            ToolStripMenuItem itm5 = new ToolStripMenuItem("Verrouiller la session");
            ToolStripMenuItem itm6 = new ToolStripMenuItem("Quitter");
            notify8Down.Icon = Properties.Resources.test;
            notify8Down.BalloonTipTitle = "8down";
            notify8Down.BalloonTipIcon = ToolTipIcon.Info;
            notify8Down.BalloonTipText = "8down est lancée.\r\nQuentin Van de Kadsye - 2014";

            contxtmnu.Items.AddRange(new ToolStripMenuItem[] { itm0, itm1, itm2 });
            contxtmnu.Items.Add(new ToolStripSeparator());
            contxtmnu.Items.AddRange(new ToolStripMenuItem[] { itm4,itm5 });
            contxtmnu.Items.Add(new ToolStripSeparator());
            contxtmnu.Items.AddRange(new ToolStripMenuItem[] { itm6 });
            itm0.Click += itm0_Click;
            itm1.Click += itm1_Click;
            itm2.Click += itm2_Click;
            //itm3.Click += itm3_Click;
            itm4.Click += itm4_Click;
            itm5.Click += itm5_Click;
            itm6.Click += itm6_Click;
            notify8Down.ContextMenuStrip = contxtmnu;
            notify8Down.Visible = true;
            notify8Down.ShowBalloonTip(0);
            Application.Run();
            
            
            
        }

        static void itm6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        static void itm5_Click(object sender, EventArgs e)
        {
            Process processShut = new Process();
            processShut.StartInfo.FileName=@"C:\Windows\System32\rundll32.exe";
            processShut.StartInfo.CreateNoWindow = true;
            processShut.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processShut.StartInfo.Arguments = "user32.dll,LockWorkStation";
            processShut.Start();

        }

        static void itm4_Click(object sender, EventArgs e)
        {
            Process processShut = new Process();
            processShut.StartInfo.FileName = @"C:\Windows\System32\shutdown.exe";
            processShut.StartInfo.CreateNoWindow = true;
            processShut.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processShut.StartInfo.Arguments = "/l /t 00";
            processShut.Start();
        }

        static void itm3_Click(object sender, EventArgs e)
        {
            Process processShut = new Process();
            processShut.StartInfo.FileName = @"C:\Windows\System32\shutdown.exe";
            processShut.StartInfo.CreateNoWindow = true;
            processShut.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

        }

        static void itm2_Click(object sender, EventArgs e)
        {
            Process processShut = new Process();
            processShut.StartInfo.FileName = @"C:\Windows\System32\shutdown.exe";
            processShut.StartInfo.CreateNoWindow = true;
            processShut.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processShut.StartInfo.Arguments = "/r /o /t 00";
            processShut.Start();
        }

        static void itm1_Click(object sender, EventArgs e)
        {
            Process processShut = new Process();
            processShut.StartInfo.FileName = @"C:\Windows\System32\shutdown.exe";
            processShut.StartInfo.CreateNoWindow = true;
            processShut.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processShut.StartInfo.Arguments = "/r /t 00";
            processShut.Start();
        }

        static void itm0_Click(object sender, EventArgs e)
        {
            Process processShut = new Process();
            processShut.StartInfo.FileName = @"C:\Windows\System32\shutdown.exe";
            processShut.StartInfo.CreateNoWindow = true;
            processShut.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processShut.StartInfo.Arguments = "/s /t 00";
            processShut.Start();
        }
    }
}
